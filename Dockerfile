FROM docker.io/library/debian:12

USER root

# install Minecraft dependencies
RUN apt-get -q update && \
    apt-get -q -y upgrade

RUN apt-get install --no-install-recommends -y \
    default-jre-headless \
    unzip \
    curl \
    locales

RUN update-ca-certificates -f

# clean up
RUN apt-get clean
RUN rm -rf /tmp/* /tmp/.[!.]* /tmp/..?*  /var/lib/apt/lists/*

# Setup locale
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen

# Configure environment
ENV FORGE_VERSION 1.20.1-47.2.0
ENV SHELL /bin/bash
ENV APPUSER minecraft
ENV HOME /home/$APPUSER
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

# Create minecraft user to install and run the app
RUN useradd -m -s /bin/bash -N -r $APPUSER

USER $APPUSER

# download and unpack Minecraft
WORKDIR $HOME
RUN curl -LOs https://maven.minecraftforge.net/net/minecraftforge/forge/$FORGE_VERSION/forge-$FORGE_VERSION-installer.jar

# run Minecraft installer
RUN java -jar forge-$FORGE_VERSION-installer.jar --installServer
RUN rm forge-$FORGE_VERSION-installer.jar
RUN echo "eula=true" > eula.txt

# Fix permissions so the server can be started as another user
RUN chmod 755 run.sh

EXPOSE 25565

VOLUME /data
RUN ln -s /data/server.properties
RUN ln -s /data/world
RUN ln -s /data/mods

# Run cmd as root so it can change the UID of minecraft on the fly.
USER root
COPY cmd.sh /usr/local/bin/cmd.sh
CMD ["/usr/local/bin/cmd.sh"]
