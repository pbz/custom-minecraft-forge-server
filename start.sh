#!/bin/sh

exec docker run --init --rm -it -d -p 25565:25565 --name minecraft --mount "type=bind,src=/home/minecraft/data,dst=/data" --env USER_ID=$(id -u) registry.gitlab.com/pbz/custom-minecraft-forge-server:latest
