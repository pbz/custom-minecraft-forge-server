#!/bin/bash

if [[ -n "$USER_ID" ]]; then
	usermod -u $USER_ID minecraft
fi

su -l minecraft -c "./run.sh"
